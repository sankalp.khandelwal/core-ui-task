import logo from './logo.svg';
import './App.css';
import Drawer from './components/Drawer';
import NavBar from './components/NavBar';
import CardsCharts from './components/CardsCharts';
import BiggerChart from './components/BiggerChart';
import TrafficSaleNdTable from './components/TrafficSaleNdTable';
import MiddleCards from './components/MiddleCards';
import DasboardMain from './components/DasboardMain';
import Footer from './components/Footer';
import Test from './components/Test';

function App() {
  return (
    <div className="App">
      {/* <TrafficSaleNdTable/> */}
      {/* <MiddleCards/> */}
       <Drawer/>
      {/* <NavBar /> */}
      {/* <CardsCharts/> */}
      {/* <BiggerChart/> */}
      {/* <DasboardMain/> */}
      {/* <Footer/> */}

      {/* <Test/> */}
    </div>
  )
}

export default App;
