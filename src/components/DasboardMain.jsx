import React from 'react'
import BiggerChart from './BiggerChart'
import CardsCharts from './CardsCharts'
import Drawer from './Drawer'
import MiddleCards from './MiddleCards'
import NavBar from './NavBar'
import TrafficSaleNdTable from './TrafficSaleNdTable'

function DasboardMain() {
  return (
    <div>
<Drawer/>
<NavBar />
<CardsCharts/>
<BiggerChart/>
<MiddleCards/>
<TrafficSaleNdTable/>


    </div>
  )
}

export default DasboardMain