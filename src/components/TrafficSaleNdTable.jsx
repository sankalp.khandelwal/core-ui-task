import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import './TrafficSaleNdTable.css';
import { GrTwitter, GrUser, GrUserFemale } from "react-icons/gr";
import { BsFacebook, BsGoogle } from 'react-icons/bs';
import { SiLinkedin } from 'react-icons/si';
import { FiUsers } from 'react-icons/fi';



function TrafficSaleNdTable() {

  const slelector = useSelector((state) => state.TableReducer);
  return (
    <div>
      <div className='mainContainrTrfcSales'>

        <div className='firstTopMainBox'>
          <div className='suprHedingBox'>
            <span className='spanHedingOnly'> Traffic & Sales</span>
          </div>
          <div className='clentBox'>
            <div className='cleintTxtBix'>
              <div className='innerBox'>
                <div className='innerTxtBox'>
                  <div className='wordTxtBox'> <span className='txtClinsdBox'>New Clients</span></div>
                  <div className='NumBox'> <span className='numSpaninsidBox'> <strong> 9,123</strong> </span></div>
                </div>
              </div>

            </div>
            <div className='cleintTxtBix'>
              <div className='innerBox2'>
                <div className='innerTxtBox'>
                  <div className='wordTxtBox'> <span className='txtClinsdBox'>New Clients</span></div>
                  <div className='NumBox'> <span className='numSpaninsidBox'> <strong> 9,123</strong> </span></div>
                </div>
              </div>

            </div>
            <div className='cleintTxtBix'>
              <div className='innerBox3'>
                <div className='innerTxtBox'>
                  <div className='wordTxtBox'> <span className='txtClinsdBox'>New Clients</span></div>
                  <div className='NumBox'> <span className='numSpaninsidBox'> <strong> 9,123</strong> </span></div>
                </div>
              </div>
            </div>
            <div className='cleintTxtBix'>
              <div className='innerBox4'>
                <div className='innerTxtBox'>
                  <div className='wordTxtBox'> <span className='txtClinsdBox'>New Clients</span></div>
                  <div className='NumBox'> <span className='numSpaninsidBox'> <strong> 9,123</strong> </span></div>
                </div>
              </div>
            </div>
          </div>
          <div className='lstPrgrBarBox'>

            <div className='prgrBarBoxLeftinside'>
              <div className='prgrBarndTxt'>
                <span className='txtClinsdBox'>Monday</span>
                <div className='filrBoxPrnt'>
                  <div className='firstBckBox'> <div className='fillrBox1'> </div> </div>
                  <div className='firstBckBox'> <div className='fillrBox2'></div> </div>
                </div>
              </div>

              <div className='prgrBarndTxt'>
                <span className='txtClinsdBox'>tuesday</span>
                <div className='filrBoxPrnt'>
                  <div className='firstBckBox'> <div className='fillrBox1'> </div> </div>
                  <div className='firstBckBox'> <div className='fillrBox2'></div> </div>
                </div>
              </div>

              <div className='prgrBarndTxt'>
                <span className='txtClinsdBox'>Wednesday</span>
                <div className='filrBoxPrnt'>
                  <div className='firstBckBox'> <div className='fillrBox1'> </div> </div>
                  <div className='firstBckBox'> <div className='fillrBox2'></div> </div>
                </div>
              </div>

              <div className='prgrBarndTxt'>
                <span className='txtClinsdBox'>Thursday</span>
                <div className='filrBoxPrnt'>
                  <div className='firstBckBox'> <div className='fillrBox1'> </div> </div>
                  <div className='firstBckBox'> <div className='fillrBox2'></div> </div>
                </div>
              </div>

              <div className='prgrBarndTxt'>
                <span className='txtClinsdBox'>Friday</span>
                <div className='filrBoxPrnt'>
                  <div className='firstBckBox'> <div className='fillrBox1'> </div> </div>
                  <div className='firstBckBox'> <div className='fillrBox2'></div> </div>
                </div>
              </div>

              <div className='prgrBarndTxt'>
                <span className='txtClinsdBox'>Saturday</span>
                <div className='filrBoxPrnt'>
                  <div className='firstBckBox'> <div className='fillrBox1'> </div> </div>
                  <div className='firstBckBox'> <div className='fillrBox2'></div> </div>
                </div>
              </div>

              <div className='prgrBarndTxt'>
                <span className='txtClinsdBox'>Sunday</span>
                <div className='filrBoxPrnt'>
                  <div className='firstBckBox'> <div className='fillrBox1'> </div> </div>
                  <div className='firstBckBox'> <div className='fillrBox2'></div> </div>
                </div>
              </div>
            </div>


            <div className='prgrBarBoxRightinside'>
              <div className='topmaleFemaleBox'>
                <div className='maleBox'>
                  <div className='iconMaleBox'> <GrUser className='iconMaleOnly' />
                    <div className='spanMaleTxtBox'>
                      <span className='spanOnlyMaleTxt'>Male</span>
                      <span className='spanOnlyMaleTxt'> <strong> 53%</strong> </span>
                    </div>
                  </div>
                  <div className='prgrBrMale'>
                    <div className='backBoxPrgrsBAr'>
                      <div className='filrBoxMale'></div>
                    </div>
                  </div>
                </div>
                <div className='femaleBox'>
                  <div className='iconMaleBox'> <GrUserFemale className='iconMaleOnly' />
                    <div className='spanMaleTxtBox'>
                      <span className='spanOnlyMaleTxt'>Female</span>
                      <span className='spanOnlyMaleTxt'><strong> 43%</strong></span>
                    </div>
                  </div>
                  <div className='prgrBrMale'>
                    <div className='backBoxPrgrsBAr'>
                      <div className='filrBoxfemale'></div>
                    </div>
                  </div>

                </div>
              </div>
              <div className='botmorganicBox'>
                <div className='organicserchBox'>
                  <div className='iconMaleBox'> <BsGoogle className='iconMaleOnly' />
                    <div className='spanMaleTxtBox'>
                      <span className='spanOnlyMaleTxt'>Organic Search</span>
                      <span className='spanOnlyMaleTxt'> <strong>191.235 (56%)</strong></span>
                    </div>
                  </div>
                  <div className='prgrBrMale'>
                    <div className='backBoxPrgrsBAr'>
                      <div className='filrBoxorganic1'></div>
                    </div>
                  </div>
                </div>

                <div className='organicserchBox'>
                  <div className='iconMaleBox'> <BsFacebook className='iconMaleOnly' />
                    <div className='spanMaleTxtBox'>
                      <span className='spanOnlyMaleTxt'>Organic Search</span>
                      <span className='spanOnlyMaleTxt'> <strong>51,223 (15%) </strong></span>
                    </div>
                  </div>
                  <div className='prgrBrMale'>
                    <div className='backBoxPrgrsBAr'>
                      <div className='filrBoxorganic2'></div>
                    </div>
                  </div>
                </div>

                <div className='organicserchBox'>
                  <div className='iconMaleBox'> <GrTwitter className='iconMaleOnly' />
                    <div className='spanMaleTxtBox'>
                      <span className='spanOnlyMaleTxt'>Organic Search</span>
                      <span className='spanOnlyMaleTxt'> <strong>37,564 (11%) </strong></span>
                    </div>
                  </div>
                  <div className='prgrBrMale'>
                    <div className='backBoxPrgrsBAr'>
                      <div className='filrBoxorganic3'></div>
                    </div>
                  </div>
                </div>

                <div className='organicserchBox'>
                  <div className='iconMaleBox'> <SiLinkedin className='iconMaleOnly' />
                    <div className='spanMaleTxtBox'>
                      <span className='spanOnlyMaleTxt'>Organic Search</span>
                      <span className='spanOnlyMaleTxt'> <strong> 27,319 (8%)</strong></span>
                    </div>
                  </div>
                  <div className='prgrBrMale'>
                    <div className='backBoxPrgrsBAr'>
                      <div className='filrBoxorganic4'></div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

        <div className='bottmBox'>
          <div className='headinguserndallBox'>
            <div className='IconBoxReduxTable'> <FiUsers className='iconReduxTblOnly' /> </div>
            <div className='userBox'> <span className='spanUserTxt'> <strong>User</strong></span></div>
            <div className='countryBox'> <span className='spanUserTxt'> <strong>Country</strong></span></div>
            <div className='usageBox'> <span className='spanUserTxt'> <strong>Usage</strong></span></div>
            <div className='paymntMethodBox'> <span className='spanUserTxt'> <strong>Payment Method</strong></span></div>
            <div className='activitystatusBox'> <span className='spanUserTxt'> <strong>Activity</strong></span></div>
          </div>


          {slelector.map((data, index) => (
            <div className='reduxTabfordata'>
              <div className='imgBox'>
                <div className='imgOnlyBox'>
                  <img className='imgOnlyUsr' src={data.profilepic} alt="" />
                </div>
              </div>
              <div className='userBox'>

                <span className='spanUsrRedux'>{data.name}</span>
                <span class="infrmtonuser"> New | Registered: Jan 1, 2021 </span>

              </div>
              <div className='countryBox'>
                <div className='flagBox'>
                  <img className='imgFlgOnly' src={data.country} alt="" />
                </div>
              </div>
              <div className='usageBox1'>
                <div className='usagePrcntgeBox'> <span><strong> {data.usage} </strong></span>
                <span class="infrmtonuser"> Jun 11, 2021 - Jul 10, 2021</span>
                </div>
                <div className='progrsbarbox'>

                  <div className='backBoxPrgrs'>
                    <div style={{ height: '100%', width: data.usage, backgroundColor: data.color }}>
                      {console.log(data.usage, " color color")}
                    </div>
                  </div>
                </div>
              </div>
              <div className='paymntMethodBox'> <span> <strong>{data.paymentMethod}</strong></span></div>
              <div className='activitystatusBox'>
              <span class="infrmtonuser"> Last login</span>
                <span> <strong> {data.activity} </strong></span>
              </div>
            </div>

          ))}

          {/* <div className='reduxTabfordata'>
            <div className='imgBox'>
              <div className='imgOnlyBox'>
                <img className='imgOnlyUsr' src="" alt="" />
              </div>
            </div>
            <div className='userBox'>

              <span className='spanUsrRedux'>Yiorgos </span>

            </div>
            <div className='countryBox'>
              <div className='flagBox'>
                <img className='imgFlgOnly' src="" alt="" />
              </div>
            </div>
            <div className='usageBox1'>
              <div className='usagePrcntgeBox'> <span><strong> 50% </strong></span></div>
              <div className='progrsbarbox'>

                <div className='backBoxPrgrs'>
                  <div style={{ width: '', backgroundClip: '' }}></div>
                </div>
              </div>
            </div>
            <div className='paymntMethodBox'></div>
            <div className='activitystatusBox'>
              <span> <strong> 10 sec ago</strong></span>
            </div>
          </div> */}

        </div>

      </div>

    </div>
  )
}

export default TrafficSaleNdTable