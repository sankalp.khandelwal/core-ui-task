import React from 'react'
import './BiggerChart.css';
import { CChart } from '@coreui/react-chartjs'

function BiggerChart() {
  return (
    <div >

      <div className="MaincontnrChart">

        <div className="HeaderChartBox">
          <div className="headrTopLeftBox">
            <div className="traficTxtBox"> <span className="spanTraficTxt">Traffic</span></div>
            <div className="monthsTxtBox"> <span className="spanMonthsTxt"> January - July 2021</span></div>
          </div>
          <div className="headrToprightBox">
            <div className="emptyDiv"></div>
            <div className='btnsMonthsBox'>
              <div className='btnsOnlymnths'>
                <button className='dayBtn'><span className='btntxtspanOnly'>Day</span></button>
                <button className='monthbuttnonly'><span className='btntxtspanOnly'>Months</span></button>
                <button className='yearBtn'><span className='btntxtspanOnly'>Year</span></button>

              </div>
             
             
             
            </div>
            {/* <div className='btnsOnlycloud'></div> */}
          </div>

        </div>
        <div className="chartOnly">
          <CChart
            type="line"
            className='bigerChartOnlyclas'
            
            data={{
              labels: ["January", "February", "March", "April", "May", "June", "July"],
              datasets: [
                {
                  label: "My First dataset",
                  backgroundColor: "rgba(220, 220, 220, 0.2)",
                  borderColor: "rgba(220, 220, 220, 1)",
                  pointBackgroundColor: "rgba(220, 220, 220, 1)",
                  pointBorderColor: "#fff",
                  data: [40, 20, 12, 39, 10, 40, 39, 80, 40]
                },
                {
                  label: "My Second dataset",
                  backgroundColor: "rgba(151, 187, 205, 0.2)",
                  borderColor: "rgba(151, 187, 205, 1)",
                  pointBackgroundColor: "rgba(151, 187, 205, 1)",
                  pointBorderColor: "#fff",
                  data: [50, 12, 28, 29, 7, 25, 12, 70, 60],
                  // borderWidth:'90%'
                  // data: [100, 102 , 103 , 104 ,107 ,200 , 240 , 270 , 280 ]
                  
                },
              ],
            }}

          />
        </div>
        <div className="visitUniqBox">

        <div className='insideMAinBoxes'>
          <div className='TxtBox'> <span className='spanTxt'>Visits</span> </div>
          <div className='PrcntgBox'> <span className='spanPrctnge'><strong>29.703&nbsp;Users&nbsp;(40%)</strong></span></div>
          <div className='ProgrsbrBox'> 
          <div className='BackDivPrgrs'>
            <div className='fillrClrBox'></div>
          </div>
          </div>
        </div>


        <div className='insideMAinBoxes'>
        <div className='TxtBox'> <span className='spanTxt'>Unique</span> </div>
          <div className='PrcntgBox'> <span className='spanPrctnge'><strong>24.093&nbsp;Users&nbsp;(20%)</strong></span></div>
          <div className='ProgrsbrBox'> 
          <div className='BackDivPrgrs'>
            <div className='fillrClrBox2'></div>
          </div>
          </div>
        </div>
        <div className='insideMAinBoxes'>
        <div className='TxtBox'> <span className='spanTxt'>Pageviews</span> </div>
          <div className='PrcntgBox'> <span className='spanPrctnge'><strong>78.706&nbsp;Views&nbsp;(60%)</strong></span></div>
          <div className='ProgrsbrBox'> 
          <div className='BackDivPrgrs'>
            <div className='fillrClrBox3'></div>
          </div>
          </div>
        </div>
        <div className='insideMAinBoxes'>
        <div className='TxtBox'> <span className='spanTxt'>New Users</span> </div>
          <div className='PrcntgBox'> <span className='spanPrctnge'><strong>22.123&nbsp;Users&nbsp;(80%)</strong></span></div>
          <div className='ProgrsbrBox'> 
          <div className='BackDivPrgrs'>
            <div className='fillrClrBox4'></div>
          </div>
          </div>
        </div>
        <div className='insideMAinBoxes'>
        <div className='TxtBox'> <span className='spanTxt'>Bounce Rate</span> </div>
          <div className='PrcntgBox'> <span className='spanPrctnge'><strong>Average Rate&nbsp;(40.15%)</strong></span></div>
          <div className='ProgrsbrBox'> 
          <div className='BackDivPrgrs'>
            <div className='fillrClrBox5'></div>
          </div>
          </div>
        </div>
        </div>
       
      </div>

    </div>
  )
}

export default BiggerChart