import React, { useState } from 'react'
import './Drawer.css';
import { BsBell, BsDroplet, BsFileEarmarkPerson, BsFileEarmarkPersonFill, BsHexagon, BsPencil, BsSpeedometer2 } from "react-icons/bs";
import { AiFillShop, AiOutlineCalculator, AiOutlineCopyrightCircle, AiOutlineStar } from 'react-icons/ai';
import { BiLeftArrow } from 'react-icons/bi'
import { FaWpforms } from 'react-icons/fa'
import { CiAlarmOff } from 'react-icons/ci'
import { useRef } from 'react';
import NavBar from './NavBar';
import CardsCharts from './CardsCharts';
import BiggerChart from './BiggerChart';
import TrafficSaleNdTable from './TrafficSaleNdTable';
import MiddleCards from './MiddleCards';
import Footer from './Footer';



function Drawer(props) {

  const alemen = useRef("");

const [check, setCheck] = useState(false);
 
 const navtoggle =()=>{
  setCheck(!check);

  if(check===true){
  
    console.log('sankalp,,,,,,,.........')
    document.getElementById('maincontrnId').style.width ="0px";
    document.getElementById('holdingcont').style.width= "100vw";
    document.getElementById('holdingcont').style.left= '0px';
    document.getElementById('maincontrnId').style.transitionDuration= "0.5s";
    document.getElementById('holdingcont').style.transitionDuration= "0.5s";
    document.getElementById('firstCoreIConD').style.width= "0px";

  
  }else if(check===false){
    console.log('sankalp,,,,,,,.........')
    document.getElementById('maincontrnId').style.width = "17%";
    document.getElementById('holdingcont').style.width= "83vw";
    document.getElementById('holdingcont').style.left= '220px';
    document.getElementById('maincontrnId').style.transitionDuration= "0.5s";
    document.getElementById('holdingcont').style.transitionDuration= "0.5s";
    document.getElementById('firstCoreIConD').style.width= "90%";

  }
 }
 
 

  return (
    <div style={{ backgroundColor: 'var(--cui-light,#ebedef)', overflow:'hidden', boxSizing:'border-box', height:'1500vh'}}>
      <div  className=" mainDrawerContnr" id="maincontrnId">

        {/* <div className='firstCoreIConDiv' id='firstCoreIConD'> <AiOutlineCopyrightCircle className='iconCpyright' />
        <div className='coruiSpanBox'><span className='spanOnlycore'>CORE</span> <span className='uispanOnly'>UI</span></div>
        <div><span className='reactClassSpan'>REACT.JS</span></div>
        </div> */}

        <div className='dashboardthemendCompoMainContner'>
          <div className='DasbordTxtBox'> <BsSpeedometer2 className='speedoMetrIcon' /> <span className='spanDasboardtxtDrwer'> Dasboard</span>
            <button className='newBtnonly'><span className='btnNewTxtSpan'><strong> NEW</strong> </span></button>
          </div>

          <div className='ThemeTxtBox'><span className='themeTxtSpan'><strong>THEME</strong> </span></div>
          <div className='colrBox'> <BsDroplet className='speedoMetrIcon' /> <div className='emptyBox'></div>
            <span className='spancolorTxt'>Color</span>

          </div>
          <div className='colrBox'> <BsPencil className='speedoMetrIcon' />  <div className='emptyBox'></div>
            <span className='spancolorTxt'>Typography</span>
          </div>

          <div className='ThemeTxtBox'> <span className='themeTxtSpan'><strong>COMPONENTS</strong> </span></div>
          <div className='colrBox'> <AiFillShop className='speedoMetrIcon' /> <div className='emptyBox'></div>
            <span className='spancolorTxt'>Base</span>
          </div>

          <div className='colrBox'> <BiLeftArrow className='speedoMetrIcon' /> <div className='emptyBox'></div>
            <span className='spancolorTxt'>Button</span>
          </div>

          <div className='colrBox'> <FaWpforms className='speedoMetrIcon' />
            <div className='emptyBox'></div>
            <span className='spancolorTxt'>Forms</span>
          </div>

          <div className='colrBox'> <CiAlarmOff className='speedoMetrIcon' />
            <div className='emptyBox'></div>
            <span className='spancolorTxt'>Charts</span>
          </div>

          <div className='colrBox'> <AiOutlineStar className='speedoMetrIcon' />
            <div className='emptyBox'></div>
            <span className='spancolorTxt'>Icons</span>
          </div>

          <div className='colrBox'><BsBell className='speedoMetrIcon' />
            <div className='emptyBox'></div>
            <span className='spancolorTxt'>Notification</span>
          </div>


          <div className='colrBox'> <AiOutlineCalculator className='speedoMetrIcon' />
            <div className='emptyBox'></div>
            <span className='spancolorTxt'>Widget</span>
            <div className='empty2'> </div>
            <button className='newBtnonly'><span className='btnNewTxtSpan'><strong> NEW</strong> </span></button>
          </div>
          <div className='ThemeTxtBox'><span className='themeTxtSpan'><strong>Extra</strong> </span></div>

          <div className='colrBox'> <AiOutlineStar className='speedoMetrIcon' />
            <div className='emptyBox'></div>
            <span className='spancolorTxt'>Pages</span>
          </div>
        </div>

        {/* <div className='firstCoreIConDiv'> <BsHexagon /></div> */}

      </div>
      <div className='HoldingContainer'  id="holdingcont">
        <NavBar navtoggle={navtoggle} />
        <CardsCharts />
        <BiggerChart />
        <MiddleCards />
        <TrafficSaleNdTable />
        <Footer/>


      </div>
    </div>
  )
}

export default Drawer