import React from 'react'
import './Footer.css';


function Footer() {
  return (
    <div>
        <div className='mainContFoter'>
            <div className='boxleft'> <a href="">CoreUI &nbsp;</a> <span>© 2021 creativeLabs.</span></div>
            <div className='boxRight'> <span>Powered by &nbsp;</span> <a href=""> CoreUI for React</a></div>
        </div>
    </div>
  )
}

export default Footer