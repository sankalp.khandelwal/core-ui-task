import React, { useState } from 'react'
import './NavBar.css';
import { AiOutlineCopyrightCircle, AiOutlineMenu } from 'react-icons/ai';
import { BiBell } from "react-icons/bi";
import { BsListTask } from "react-icons/bs";
import { TfiEmail } from "react-icons/tfi";
import CardsCharts from './CardsCharts';
import BiggerChart from './BiggerChart';
import Drawer from './Drawer';
import { useRef } from 'react';

function NavBar(props) {


    const sidevbaropen = () => {

        props.navtoggle();
    }
    return (
        <div >
            {/* <Drawer />  */}
            <div className="MainNavContnr">

                <div className="NavTopBox">
                    <div className="barIconBox"> < AiOutlineMenu onClick={sidevbaropen} style={{ fontSize: '21px', color: 'rgba(44,56,74,.681)' }} /></div>

                    <div className="leftTextBtnMainBox">
                        <div className="txtBOx1"> <span className="spanTagCLass"> Dashboard</span></div>
                        <div className="txtBOx2"> <span className="spanTagCLass"> User</span></div>
                        <div className="txtBOx3"> <span className="spanTagCLass">Settings</span></div>


                    </div>

                    <div className='firstCoreIConDiv' id='firstCoreIConD'>
                        <div className='insideCoreuifirstbox'> <AiOutlineCopyrightCircle className='iconCpyright' /></div>
                        <div className='coruiSpanBox'><span className='spanOnlycore'>CORE</span> <span className='uispanOnly'>UI</span></div>
                        <div className='reactjstxtBox'><span className='reactClassSpan'>REACT.JS</span></div>
                    </div>

                    <div className="EmptyBox1"></div>
                    <div className="RightIconBtnMainBox">
                        <div className="IconBox1"> <BiBell className="iconsOnlyRight" /></div>
                        <div className="IconBox1"> <BsListTask className="iconsOnlyRight" /></div>
                        <div className="IconBox1"> <TfiEmail className="iconsOnlyRight" /></div>
                        <div className="IconBox2"> <img className="iconImg" src="https://coreui.io/demos/react/4.0/free/static/media/8.35ee8919.jpg" alt="" /></div>
                    </div>
                </div>


                <div className="NavBtomBox">
                    <div className="leftAnchorTagBox">
                        <div> <span><a href="">Home </a></span> </div>
                        <div><span className="spanTagCLass"> /  Dasboard</span></div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default NavBar